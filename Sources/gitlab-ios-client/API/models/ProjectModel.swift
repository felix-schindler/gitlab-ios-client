//
//  File.swift
//  
//
//  Created by Felix Schindler on 09.11.22.
//

import Foundation

protocol Model {
    var apiSlug: String { get }
}

class ProjectModel : Model {
    public let apiSlug: String = "/projects"

    public static func list(starred: Bool = false) async -> [Project]? {
        return nil
    }
    
    public func branches(_ projectId: Int) async -> [Branch]? {
        return API.get(type: [Branch].self, endpoint: "/projects/\(projectId)/repository/branches") ?? nil
    }
    
//    public func star() async -> Bool {
//        API.post(type: ToggleStar, endpoint: <#T##String#>)
//        API.req(type: ToggleStar, url: "https://gitlab.com/felix-schindler/gitlab-ios/toggle_star.json", method: .post)
//    }
}
