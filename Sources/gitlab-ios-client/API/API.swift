//
//  API.swift
//  
//
//  Created by Felix Schindler on 08.11.22.
//

import Foundation
import SwiftUI
import SwiftHttp

class API {
    @AppStorage("domain") public static var domain: String = "https://gitlab.com"
    @AppStorage("api_base") public static var base: String = "api/v4"
    @AppStorage("token") public static var token: String = ""
    
    private static let client: HttpClient = UrlSessionHttpClient(log: false)
    private static let decoder = JSONDecoder()

    public static func req<T: Codable>(type: T.Type, method: HttpMethod, endpoint: String, query: Dictionary<String, String> = [:], body: Dictionary<String, String> = [:]) async -> T? {
        do {
            // FIXME: Set HttpUrl(string: endpoint).trainlingSlashEnabled to `false`
            let httpUrl = endpoint.starts(with: "https://") ? HttpUrl(string: endpoint) : HttpUrl(host: domain, path: [base, endpoint], query: query, trailingSlashEnabled: false)
            
            if (httpUrl != nil) {
                var reqBody: Data? = nil
                if (!body.isEmpty) {
                    reqBody = try JSONEncoder().encode(body)
                }
            
                let req = HttpRawRequest(url: httpUrl!,
                                         method: method,
                                         headers: [
                                            .authorization: "Bearer \(token)",
                                            .contentType: "application/json"
                                         ],
                                         body: reqBody)
            
                let response = try await client.dataTask(req)

                decoder.keyDecodingStrategy = .convertFromSnakeCase
                decoder.dateDecodingStrategy = .custom(iso8601Decoder())

                return try decoder.decode(T.self, from: response.data)
            }
        } catch {
            print(error)
        }
        return nil
    }
    
    public static func get<T: Codable>(type: T.Type, endpoint: String, query: Dictionary<String, String> = [:]) async -> T? {
        return await API.req(type: type, method: .get, endpoint: endpoint, query: query)
    }
    
    public static func post<T: Codable>(type: T.Type, endpoint: String, query: Dictionary<String,String> = [:], body: Dictionary<String, String> = [:]) async -> T? {
        return await API.req(type: type, method: .post, endpoint: endpoint, query: query, body: body)
    }
}


enum DateError: Error {
    case invalidDate
}

/// Decodes iso8601 date Strings to object
func iso8601Decoder() -> @Sendable (Decoder) throws -> Date {
    { (decoder) -> Date in
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)

        let container = try decoder.singleValueContainer()
        let dateStr = try container.decode(String.self)

        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        if let date = formatter.date(from: dateStr) {
            return date
        }
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXXXX"
        if let date = formatter.date(from: dateStr) {
            return date
        }
        throw DateError.invalidDate
    }
}
