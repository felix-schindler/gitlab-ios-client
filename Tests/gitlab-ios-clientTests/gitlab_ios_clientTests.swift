import XCTest
@testable import gitlab_ios_client

final class gitlab_ios_clientTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(gitlab_ios_client().text, "Hello, World!")
    }
}
